Code Repository to 'Handbook of Human-Machine Systems - Chapter Hybrid Intelligence: Augmenting employees’ decision-making with AI-based applications' IEEE

-------------------------------------------------------------------------

## Hybrid Intelligence: Augmenting employees’ decision-making with AI-based applications
__Ina Heine, Thomas Hellebrandt, Louis Huebser, and Marcos Padrón__
<br>  
_Laboratory for Machine Tools and Production Engineering, RWTH Aachen University_
<br>  
Abstract: _Recent advances in the field of Artificial Intelligence (AI) have led to increased
practical relevance and implementation interest of this technology in industry. Especially
in the service area, characterized by direct customer interaction and high work stress, AI-
based applications could have positive effects on employees’ decision-making and stress
reduction. One of the main implementation challenges is raising technology acceptance
without creating overreliance. We follow a human-centered design approach to address
this challenge and present a technical use case for the service area that demonstrates human decision-making augmentation with AI-based applications. The company in question
for is a call centre service provider offering its services to original equipment manufacturers (OEMs) in the telecommunications, media and technology sectors._
<br>  
__Keywords:__ Augmented Intelligence, cognitive engineering, human-machine decision-making